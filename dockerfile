FROM ubuntu:latest
MAINTAINER Dmitry Davidov "dmitrii.davidov@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python-pip libxml2-dev libxslt1-dev zlib1g-dev python-dev build-essential
ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt
CMD python manage.py run_server