# coding: utf-8

import os


base_dir = os.path.abspath(os.path.dirname(__file__))

files_dir = os.path.join(base_dir, '_files')
if not os.path.exists(files_dir):
    os.makedirs(files_dir)


DEBUG = True

SECRET_KEY = '__random_string|50__'
CSRF_ENABLED = True

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(files_dir, 'db.sqlite')
SQLALCHEMY_TRACK_MODIFICATIONS = True

CELERY_BROKER_URL = 'sqla+sqlite:///' + os.path.join(files_dir, 'celery_tasks.sqlite')  # TODO: make it inmemory db
CELERY_RESULT_BACKEND = 'db+sqlite:///' + os.path.join(files_dir, 'celery_results.sqlite')  # TODO: maybe make it inmemory db too?
CELERY_ACCEPT_CONTENT = ['json', 'msgpack', 'yaml']
CELERY_TRACK_STARTED = True
CELERYD_CONCURRENCY = 1


CACHE_TYPE = 'simple'


del os
