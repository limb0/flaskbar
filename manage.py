#! /usr/bin/env python

# coding: utf-8

from flask_script import Manager, Shell

from __app_module__ import app, celery, db


manager = Manager(app, with_default_commands=False)
manager.add_command('shell', Shell())


@manager.command
def run_server():
    """Runs the Flask development server."""
    app.run(host='0.0.0.0')


@manager.command
def run_celery():
    """Runs Celery worker."""
    from celery.bin import worker

    worker = worker.worker(app=celery)
    worker.run()


@manager.command
def init_db():
    """Initializes the database with dafault data."""


if __name__ == '__main__':
    db.create_all()

    manager.run()
