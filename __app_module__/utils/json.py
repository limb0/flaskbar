# coding: utf-8

from flask.json import JSONEncoder


class JsonEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, JsonSerializable):
            return obj.json_serialize()
        return super(JsonEncoder, self).default(obj)


class JsonSerializable(object):

    def json_serialize(self):
        raise NotImplementedError
