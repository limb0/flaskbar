# coding: utf-8

from flask import Flask

from .utils.json import JsonEncoder


app = Flask(__name__, static_url_path='',
            static_folder='static', template_folder='templates')
app.config.from_object('config')
app.json_encoder = JsonEncoder


from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy(app)


from celery import Celery


def make_celery(app):
    celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)

    TaskBase = celery.Task

    class ContextTask(TaskBase):
        """  """
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask

    return celery


celery = make_celery(app)


from .blueprints.api import mod as api_mod


app.register_blueprint(api_mod)


from . import views
