# coding: utf-8

from flask import jsonify

from . import mod


@mod.route('/')
def books():
    return jsonify(data='This is an API.')
